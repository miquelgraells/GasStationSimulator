/*
	Copyright 2016  Miquel Graells Monreal <miquelgraells@hotmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

var App = angular.module('app', []);
App.controller('appController', function controller($scope) {
	// menu states
	$scope.menu = [true,false,false,false,false]; 
	//change active menu when users clicks one
	$scope.stateMenu = function (index){
		for (var i = 0; i < $scope.menu.length; i++) {
			$scope.menu[i] = (i == index) ? true : false;			
		}
		if (index == 4){
			applyAllStrategies();
		}
	}
	// array to hold markers that represents gas station on the map
	$scope.arrStationsMarkers = new Array();
	// array to hold the gastations objects
	$scope.arrGasStations = new Array();
	// to know if user is editing a station
	$scope.edittingStation = false; 
	// to kown wich station in the array the user is editting
	$scope.indexStation = 0; 
	// array to hold the cars objects
	$scope.arrCars = new Array();
	// to know if user is editing a car
	$scope.edittingCar = false; 
	// to kown wich car in the array the user is editting
	$scope.indexCar = 0; 
	// array to hold the stages objects
	$scope.arrStages = new Array();
	$scope.routeDistance = 0;
	// all the stategies
	$scope.arrStrategies = ["sin planificacion","tiempo minimo","consumo minimo"];
	
	// set the route and the gas satation on the map
	// the sataion are located dividing the distance between the number of gas stations +1
	$scope.setRoute = function(){
		numGasStations = parseInt($("#numGasStations").val());
		if(numGasStations > 0) {
			calculateAndDisplayRoute(directionsService, directionsDisplay);
		} else {
			alert("You must set a number of gas stations");
		}
	}

	$scope.saveCar = function() { 	
		if($scope.edittingCar){
			// if the car its modified then whe change the values of the car
			$scope.arrCars[$scope.indexCar].consume = $scope.consume;
			$scope.arrCars[$scope.indexCar].maxFuel = $scope.maxFuel;
			$scope.arrCars[$scope.indexCar].inicialFuel = $scope.inicialFuel;
			$scope.arrCars[$scope.indexCar].velocity = $scope.velocity;
		} else {
			var numCars = parseInt($scope.numCars);
			for (var i = 0 ; i < numCars; i++) {	
				var car = {
					'consume' : parseFloat($scope.consume),
					'maxFuel' : parseInt($scope.maxFuel),
					'inicialFuel' : parseInt($scope.inicialFuel),
					'velocity' : parseInt($scope.velocity),
					'currentFuel' : parseInt($scope.inicialFuel)
				};
				$scope.arrCars.push(car);
			}
		}			
	}

	$scope.removeCar = function(){
		$scope.arrCars.splice($scope.indexCar, 1);	
	}

	$scope.editCar = function(index) {
		$scope.edittingCar = true;		
		$scope.indexCar = index;
		$('#consume').val($scope.arrCars[index].consume);
	 	$('#maxFuel').val($scope.arrCars[index].maxFuel);
	 	$('#inicialFuel').val($scope.arrCars[index].inicialFuel);
	 	$('#velocity').val($scope.arrCars[index].velocity);	 	
	}

	// when user closes the modal by clicking outside the modal
	$('#carModal').on('hidden.bs.modal', function(e){		
	 	$scope.edittingCar = false;	 	
	 	$('#consume').val("");
	 	$('#maxFuel').val("");
	 	$('#inicialFuel').val("");
	 	$('#velocity').val("");
	 	$('#numCars').val("");
	});

	$scope.saveStation = function() { 	
		if($scope.edittingStation){
			$scope.arrGasStations[$scope.indexStation].timeService = parseInt($('#timeService').val());
			$scope.arrGasStations[$scope.indexStation].price = parseFloat($('#priceFuel').val());
			$scope.arrGasStations[$scope.indexStation].quantity = parseInt($('#fuelStation').val());
			$scope.arrGasStations[$scope.indexStation].location.lat = parseInt($('#stationLat').val());
			$scope.arrGasStations[$scope.indexStation].location.lng = parseInt($('#stationLng').val());
		} 		
	}

	$scope.editStation = function(index) {
		$scope.edittingStation = true;		
		$scope.indexStation = index;
		$('#timeService').val($scope.arrGasStations[index].timeService);
	 	$('#priceFuel').val($scope.arrGasStations[index].price);
	 	$('#fuelStation').val($scope.arrGasStations[index].quantity);
	 	$('#stationLat').val($scope.arrGasStations[index].location.lat);	 	
	 	$('#stationLng').val($scope.arrGasStations[index].location.lng);	 	
	}

	// when user closes the modal by clicking outside the modal
	$('#stationModal').on('hidden.bs.modal', function(e){		
	 	$scope.edittingStation = false;	 	
	 	$('#timeService').val("");
	 	$('#priceFuel').val("");
	 	$('#fuelStation').val("");
	 	$('#stationLat').val("");
	 	$('#stationLng').val("");
	});

	// when user closes the modal by clicking outside the modal
	$('#markerModal').on('hidden.bs.modal', function(e){			 	
	 	$('#markerLat').val("");
	 	$('#markerLng').val("");
	});

	$scope.removeStation = function(){
		$scope.arrGasStations.splice($scope.indexStation, 1);	
		// remove station marker
		$scope.arrStationsMarkers[$scope.indexStation].setMap(null);
		$scope.arrStationsMarkers.splice($scope.indexStation, 1);
		// TO DO when one stage is deleted two stages must be added
	}

	// to get coordinates given a place name
	geocoder = new google.maps.Geocoder();
	// to autocolpit wher user search a place
	new google.maps.places.Autocomplete(document.getElementById('cityOrigin'));
	new google.maps.places.Autocomplete(document.getElementById('cityDestination'));
		
	// centring map by default in UAB. 
	// In the future try to get the user localtion and set that to cent the map
	map = new google.maps.Map(document.getElementById('map'), {
		center: new google.maps.LatLng(41.501755, 2.104606),
		zoom: 15
	}); 	
	// to set the route on the map
	var directionsDisplay = new google.maps.DirectionsRenderer;
	var directionsService = new google.maps.DirectionsService;
	//directionsDisplay.setMap(map);


	function calculateAndDisplayRoute(directionsService, directionsDisplay) {
		// get names city origion and destination
		var cityOrigin = $('#cityOrigin').val();
		var cityDestination = $('#cityDestination').val();

		// getting coordinates city origin
		geocoder.geocode({'address': cityOrigin}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				var origin = results[0].geometry.location;
				// getting coordinates city destination
				geocoder.geocode({'address': cityDestination}, function(results, status) {
					if (status === google.maps.GeocoderStatus.OK) {
						var destination = results[0].geometry.location;
						// set the route on the map
						directionsService.route({
							origin: origin,  
							destination: destination,  
							travelMode: google.maps.DirectionsTravelMode.DRIVING
						}, function(response, status) {
							if (status == google.maps.DirectionsStatus.OK) {
								directionsDisplay.setDirections(response);
								// route distace in meters
								var totalDistance = (response.routes[0].legs[0].distance.value);
								$scope.routeDistance = totalDistance/1000;								
								// travel duration in minutes
								var totalDuration = (response.routes[0].legs[0].duration.value); //min								

								//http://stackoverflow.com/questions/16180104/get-a-polyline-from-google-maps-directions-v3
								var polyline = new google.maps.Polyline({
									path: [],
									strokeColor: '#FF0000',
									strokeWeight: 3
								});
								var bounds = new google.maps.LatLngBounds();

								var legs = response.routes[0].legs;
								for (i = 0; i < legs.length; i++) {
									var steps = legs[i].steps;
									for (j = 0; j < steps.length; j++) {
										var nextSegment = steps[j].path;
										for (k = 0; k < nextSegment.length; k++) {
											polyline.getPath().push(nextSegment[k]);
											bounds.extend(nextSegment[k]);
										}
									}
								}
								polyline.setMap(map); 
								map.fitBounds(bounds);

								var coordinates = []; 
								polyline.getPath().forEach(function(latLng) { 
									coordinates.push(latLng); 
								}); 
								// by default we put the gas stations at same distance between them
								var numStages = ++numGasStations;
								var stationPerKm =  totalDistance/numStages;
								var totalKmStage = 0;
								var kmOrignStage = 0;
								var kmEndStage = 0;
								for(var i = 1; i < coordinates.length-1; i++){										
									totalKmStage += google.maps.geometry.spherical.computeDistanceBetween(coordinates[i], coordinates[i+1]);									
									// if the stage is big enought we put a gas station and stage
									if(totalKmStage >= stationPerKm){
										// to avoid putting one more gas station at the end of route, we must check
										// that we have put all the stations required. The reasos is that calculating the distance 
										// from one opint to the next we are missing some preceison, this casuses that be are putting gas stations
										// a little bit before that what should be and, this causes that rest some more distance that souldnt but its 
										// beeing calculating and puts one more station at the end.
										if($scope.arrStages.length < numStages-1){
											// put a marker on map and save it on array
											var marker = new google.maps.Marker({
									            position: coordinates[i],
									            map: map,
									            draggable: true,   
									            icon : '../public/images/gasStation.png'   
									        });
									        // to show a modal when gas station clicked on the map
									        $scope.arrStationsMarkers.push(marker);					
											 // to show a modal when gas station clicked on the map
											google.maps.event.addListener(marker, 'click', function() {
												$('#markerModal').modal('show')
												//var index =  $scope.arrStationsMarkers.indexOf(this);
												$scope.stationLat = this.getPosition().lat();//$scope.arrGasStations[index].location.lat;
												$scope.stationLng = this.getPosition().lng();//$scope.arrGasStations[index].location.lng;											
											});

									        //http://stackoverflow.com/questions/6611634/google-maps-api-v3-add-event-listener-to-all-markers						        
											google.maps.event.addListener(marker,'dragend',function(e){
												this.setPosition(find_closest_point_on_path(e.latLng,coordinates));												
											});

											google.maps.event.addListener(marker,'drag',function(e){
												this.setPosition(find_closest_point_on_path(e.latLng,coordinates));										    	
											});

									        //save the gas station info on array
									        var gasStation = {
									        	'timeService' : 20,
									        	'price' : 1.20,
									        	'quantity' : 2000,
									        	'location' : {
									        		'lat' : coordinates[i].lat(),
									        		'lng' : coordinates[i].lng()
									        	}
									        };
								        	$scope.arrGasStations.push(gasStation);								        
								    	}								        
								        // save the stage info on array
								        // the end of one stage is the total distance calculated of all stages at the moment
								        kmEndStage += totalKmStage;								        
								        var stage = {
								        	'kmOrign' : parseFloat(kmOrignStage/1000).toFixed(2),
								        	'kmEnd' : parseFloat(kmEndStage/1000).toFixed(2),
								        	'size' : parseFloat(totalKmStage/1000).toFixed(2)
								        }								        
								        $scope.arrStages.push(stage);
								        //restart the size for the new stage
										totalKmStage = 0;
										// the end of one stage its the begin of new one
										kmOrignStage = kmEndStage;	
									} 
									// --- just for debug ----
									// uncoment to see all coordinates in the map
									/*else {
										var marker = new google.maps.Marker({
								            position: coordinates[i],
								            map: map,   
								            icon : '../public/images/vertical1.png'   
								        });
									}*/
								}

							} else {
								window.alert('Directions request failed due to ' + status);
							}
						});
					
					} else {
						alert('Geocode was not successful for the following reason: ' + status);
					}
		  		});		
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
	  	});	
	}

	//http://stackoverflow.com/questions/10694378/confine-dragging-of-google-maps-v3-marker-to-polyline

	 function find_closest_point_on_path(marker,path_pts){
        distances = new Array();//Stores the distances of each pt on the path from the marker point 
        distance_keys = new Array();//Stores the key of point on the path that corresponds to a distance
        
        //For each point on the path
        $.each(path_pts,function(key, path_pt){
            //Find the distance in a linear crows-flight line between the marker point and the current path point
            var R = 6371; // km
            var dLat = toRad((path_pt.lat()-marker.lat()));
            var dLon = toRad((path_pt.lng()-marker.lng()));
            var lat1 = toRad(marker.lat());
            var lat2 = toRad(path_pt.lat());

            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            var d = R * c;
            //Store the distances and the key of the pt that matches that distance
            distances[key] = d;
            distance_keys[d] = key; 
            
        });
        //Return the latLng obj of the second closest point to the markers drag origin. If this point doesn't exist snap it to the actual closest point as this should always exist
        return (typeof path_pts[distance_keys[_.min(distances)]+1] === 'undefined')?path_pts[distance_keys[_.min(distances)]]:path_pts[distance_keys[_.min(distances)]+1];
    }
	

	function toRad(coor){		
		return coor * Math.PI / 180;
	}

	function applyAllStrategies(){
		strategySinPanificacion();
		strategyTiempoMinimo();
		/*for (var i = 0; i < arrStrategies.length; i++) {
			var strategy = arrStrategies[i]
			switch(strategy) {
			    case "sin planificacion":
			        strategySinPanificacion()
			        break;
			    case "tiempo minimo":
			        strategyTiempoMinimo()
			        break;
			    case "consumo minimo":
			        strategyConsumoMinimo()
			        break;
			    default:
			        strategySinPanificacion();
			}
		}*/
		showSpiderGraphic();
	}

	
	var strategySinPanificacionResults = {};
	$scope.resultPerCarSinPlanificacion = new Array();
	function strategySinPanificacion() {		
		for (var i = 0; i < $scope.arrCars.length; i++) {
			var car = $scope.arrCars[i]
			var totalKm = 0; //distance traveled
			var stops = 0; // times the car stops on a gas station
			var time = 0; //time car takes to do the distance
			var totalConsum = 0;
			var price = 0;
			for (var j = 0; j < $scope.arrGasStations.length; j++) {
				var gasStation = $scope.arrGasStations[j];
				var km = parseFloat($scope.arrStages[j].size);
				totalKm += km;
				if(j == $scope.arrGasStations.length-1 ){
					// if its the last gas satation also add the last trams distance
					totalKm += parseFloat($scope.arrStages[j+1].size) 
				}
				stops++;
				time += ((km / car.velocity) * 60) + gasStation.timeService; //in minutes
				var consum = km * car.consume;
				totalConsum += consum;
				if(j == 0) {
					price += (car.inicialFuel - consum) * gasStation.price;
				} else {
					price += (car.maxFuel - consum) * gasStation.price;
				}
			}

			var o = {	
				'km' : totalKm,
				'stops' : stops,
				'time' : time,
				'totalConsum' : totalConsum,
				'price' : price
			};
			$scope.resultPerCarSinPlanificacion.push(o);
		}
		// calculando la media de los resultados por cada coche
		var km = 0; 
		var stops = 0; 
		var time = 0; 
		var consum = 0; 
		var price = 0;
		for (var i = 0; i < $scope.resultPerCarSinPlanificacion.length; i++) {
			km += $scope.resultPerCarSinPlanificacion[i].km;
			stops += $scope.resultPerCarSinPlanificacion[i].stops;
			time += $scope.resultPerCarSinPlanificacion[i].time;
			consum += $scope.resultPerCarSinPlanificacion[i].totalConsum;
			price += $scope.resultPerCarSinPlanificacion[i].price;
		}

		strategySinPanificacionResults.km = (km/$scope.resultPerCarSinPlanificacion.length);
		strategySinPanificacionResults.stops = stops/$scope.resultPerCarSinPlanificacion.length;
		strategySinPanificacionResults.totalTime = time/$scope.resultPerCarSinPlanificacion.length;
		strategySinPanificacionResults.consum = consum/$scope.resultPerCarSinPlanificacion.length;
		strategySinPanificacionResults.price = price/$scope.resultPerCarSinPlanificacion.length;

		console.log(JSON.stringify(strategySinPanificacionResults));

		$scope.sinPlanificacionResults = [strategySinPanificacionResults.km,strategySinPanificacionResults.stops,
		strategySinPanificacionResults.totalTime,strategySinPanificacionResults.consum,strategySinPanificacionResults.price]
	}

	var strategyTiempoMinimoResults = {};
	$scope.resultPerCarTiempoMinimo = new Array();
	function strategyTiempoMinimo() {
		for (var i = 0; i < $scope.arrCars.length; i++) {
			var car = $scope.arrCars[i]
			var totalKm = 0; //distance traveled
			var stops = 0; // times the car stops on a gas station
			var time = 0; //time car takes to do the distance
			var totalConsum = 0;
			var price = 0;
			for (var j = 0; j < $scope.arrGasStations.length; j++) {
				var gasStation = $scope.arrGasStations[j];
				var km = parseFloat($scope.arrStages[j].size);
				totalKm += km;
				if(j == $scope.arrGasStations.length-1 ){
					// if its the last gas satation also add the last trams distance
					totalKm += parseFloat($scope.arrStages[j+1].size) 
				}				
				time += ((km / car.velocity) * 60); //in minutes
				var consum = km * car.consume;
				totalConsum += consum;

				car.currentFuel = car.currentFuel - totalConsum;
				var isBestStation = isTiempoMinimo(j,car);
				var neededConsume = car.consume * $scope.arrStages[j+1].size
				if(car.currentFuel < neededConsume || isBestStation) {
					if(stops == 0) {
						price += (car.inicialFuel - consum) * gasStation.price;					
					} else {
						price += (car.maxFuel - consum) * gasStation.price;
					}
					car.currentFuel = car.maxFuel;
					stops++;
					time += gasStation.timeService; //in minutes
				}

			}

			var o = {	
				'km' : totalKm,
				'stops' : stops,
				'time' : time,
				'totalConsum' : totalConsum,
				'price' : price
			};
			$scope.resultPerCarTiempoMinimo.push(o);
		}
		// calculando la media de los resultados por cada coche
		var km = 0; 
		var stops = 0; 
		var time = 0; 
		var consum = 0; 
		var price = 0;
		for (var i = 0; i < $scope.resultPerCarTiempoMinimo.length; i++) {
			km += $scope.resultPerCarTiempoMinimo[i].km;
			stops += $scope.resultPerCarTiempoMinimo[i].stops;
			time += $scope.resultPerCarTiempoMinimo[i].time;
			consum += $scope.resultPerCarTiempoMinimo[i].totalConsum;
			price += $scope.resultPerCarTiempoMinimo[i].price;
		}

		strategyTiempoMinimoResults.km = (km/$scope.resultPerCarTiempoMinimo.length);
		strategyTiempoMinimoResults.stops = stops/$scope.resultPerCarTiempoMinimo.length;
		strategyTiempoMinimoResults.totalTime = time/$scope.resultPerCarTiempoMinimo.length;
		strategyTiempoMinimoResults.consum = consum/$scope.resultPerCarTiempoMinimo.length;
		strategyTiempoMinimoResults.price = price/$scope.resultPerCarTiempoMinimo.length;

		console.log(JSON.stringify(strategyTiempoMinimoResults));

		$scope.tiempoMinimoResults = [strategyTiempoMinimoResults.km,strategyTiempoMinimoResults.stops,
		strategyTiempoMinimoResults.totalTime,strategyTiempoMinimoResults.consum,strategyTiempoMinimoResults.price]
	}

	function strategyConsumoMinimo() {

	}

	function isTiempoMinimo(index,car){
		var avaiableKm = car.currentFuel * car.consume 
		var stagesKm = 0;
		var flag = true;
		var result = true;
		for (var i = index; i < $scope.arrStages.length && flag; i++) {
			var size = $scope.arrStages[i+1].size
			if (stagesKm < avaiableKm ) {
				stagesKm += size;

				if($scope.arrGasStations[index].timeService > $scope.arrGasStations[i].timeService){
					result = false;
					flag = false;
				}
				
			} else {				
				flag = false;
			}
		}

		return result;
	}

	function showSpiderGraphic () {

		$(function () {

    		Highcharts.chart('spiderWeb', {

		        chart: {
		            polar: true,
		            type: 'line'
		        },

		        title: {
		            text: 'Sin planificación vs Tiempo mínimo vs Consumo mínimo',
		            x: -80
		        },

		        pane: {
		            size: '80%'
		        },

		        xAxis: {
		            categories: ['KM', 'Paradas', 'Tiempo total', 'Consumo (lts)',
		                    'Coste', 'Carburante restante'],
		            tickmarkPlacement: 'on',
		            lineWidth: 0
		        },

		        yAxis: {
		            gridLineInterpolation: 'polygon',
		            lineWidth: 0,
		            min: 0
		        },

		        tooltip: {
		            shared: true,
		            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
		        },

		        legend: {
		            align: 'right',
		            verticalAlign: 'top',
		            y: 70,
		            layout: 'vertical'
		        },

		        series: [{
		            name: 'Sin planificacion',
		            data: $scope.sinPlanificacionResults,
		            pointPlacement: 'on'
		        },{
		        	name: 'Tiempo mínimo',
		        	data: $scope.tiempoMinimoResults,
		        	pointPlacement: 'on'
		        }]

		    });
		});

	}

});